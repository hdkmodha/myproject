//
//  AppConfig.swift
//  WebCluesPracticle
//
//  Created by macbook pro on 21/07/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation

struct AppConfig {
    static let host: URLHost = .live
}
