//
//  ImageCell.swift
//  DownloadDemo
//
//  Created by macbook pro on 08/08/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(photo: Photos) {
        self.imageView.backgroundColor = UIColor(hex: photo.color ?? "")
        self.imageView.setImage(with: photo.urls?.regular, placeholderImage: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
        self.imageView.cancleRequest()
    }
}
