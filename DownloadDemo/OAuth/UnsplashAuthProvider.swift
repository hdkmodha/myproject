//
//  UnsplashAuthProvider.swift
//  DownloadDemo
//
//  Created by macbook pro on 08/08/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation

struct UnsplashAuthProvider: OAuthProvider {
    var url: URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "unsplash.com"
        components.path = "/oauth/authorize"
        components.queryItems = self.queryItems
        return components.url!
    }
    
    var callbackURLScheme: String? {
        return self.redirect_uri
    }
    
    var errorHandler: ((Error) -> Void)?
    
    func resolveToken(for queryItems: [URLQueryItem]) -> String {
        if let code = queryItems.first(where: {$0.name == "code"})?.value {
            return code
        }
        return ""
    }
    
    let redirect_uri: String = UnsplashConstant.redirectUri
    let response_type: String = "code"
    let client_id: String = UnsplashConstant.accessKey
    var scope: String = "public"
    
    var queryItems: [URLQueryItem] {
        var items: [URLQueryItem] = []
        items.append(URLQueryItem(name: "redirect_uri", value:  UnsplashConstant.redirectUri))
        items.append(URLQueryItem(name: "response_type", value:  "code"))
        items.append(URLQueryItem(name: "client_id", value:  UnsplashConstant.accessKey))
        items.append(URLQueryItem(name: "scope", value:  "public"))
        return items
    }
}
