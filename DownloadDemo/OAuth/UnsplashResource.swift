//
//  UnsplashResource.swift
//  DownloadDemo
//
//  Created by macbook pro on 08/08/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation
import Moya


class UnsplashResource {
    
    static let shared = UnsplashResource()
    
    private var provider = {
        MoyaProvider<UnsplashService>(
            plugins: [
                NetworkLoggerPlugin(configuration: NetworkLoggerPlugin.Configuration(logOptions: .verbose))
            ]
        )
    }()
    
    func photos(completion: @escaping (Swift.Result<[Photos], AppError>)->Void) {
        provider.request(.photos) { (result) in
            switch result {
            case .success(let response):
                let output = Swift.Result { try response.map([Photos].self) }
                    .mapError {
                        AppError.custom(message: $0.localizedDescription)
                }
                completion(output)
            case .failure(let error):
                completion(.failure(.custom(message: error.localizedDescription)))
            }
        }
    }
    
    func getPhtos() {
        provider.request(.getPhotos) { (result) in
                   switch result {
                   case .success(let response):
                    
                    let output = Swift.Result { try response.mapString() }
                           .mapError {
                               AppError.custom(message: $0.localizedDescription)
                       }
                    break
                    
                      
                   case .failure(let error):
                      break
                   }
               }
    }
    
    func accessToken(for code: String, completion: @escaping (Swift.Result<Void, AppError>) -> Void) {
        let request = UnsplashAccessTokenRequest(code: code)
        provider.request(.accessToken(request)) { (result) in
            let output = result.flatMap { (response)  in
                return Swift.Result { try response.map(UnsplashTokenResponse.self)}
                        .mapError { MoyaError.encodableMapping($0) }
                
            }.map { (accessToken) -> Void in
                accessToken.store()
            }.mapError {
                    AppError.custom(message: $0.localizedDescription)
            }
            completion(output)
        }
    }
}


struct PhotoBook: Codable {
    var id: Int?
    var name: String?
    var total: Int?
    var product: [Product]?
    var description: String?
    var category_slug: String?
    var parent_id: Int?
}





struct Product: Codable  {
    var id: Int?
    var caegory_id: Int?
    var nae: String?
    var slg: String?
    var prce: Int?
    var oter_price: Int?
    var viible: String?
    var enble: String?
    var prduct_image_urls: [String]?
    var template_url: String?
    var template_file_url: String?
    var version: String?
    var out_of_stock: Int?
    var out_of_stock_message: String?
    var updated_date: String?
    var display_on_home: String?
    var home_title: String?
    var designer_theme_price: Int?
    var sub_category: Int?
    var sub_category_slug: String?
    var sub_category_name: String?
}
