//
//  UnsplashTokenResponse.swift
//  DownloadDemo
//
//  Created by macbook pro on 08/08/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation

struct UnsplashTokenResponse: Codable {
    let access_token: String
    let token_type: String
    let refresh_token: String
    let scope: String
    let created_at: Int
}

extension UnsplashTokenResponse {
    static private let key = "UnsplashTokenResponse"
    
    func store() {
        do  {
            let data = try JSONEncoder().encode(self)
            UserDefaults.standard.set(data, forKey: Self.key)
        } catch {
            print(error)
        }
    }
    
    static var stored: UnsplashTokenResponse? {
        guard let data = UserDefaults.standard.value(forKey: Self.key) as? Data else {
            return nil
        }
        do {
            let result =  try JSONDecoder().decode(Self.self, from: data)
            return result
        } catch {
            print(error)
            return nil
        }
    }
}

