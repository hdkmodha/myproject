//
//  OAuthManager.swift
//  Source
//
//  Created by Neil Jain on 6/13/20.
//  Copyright © 2020 Neil. All rights reserved.
//

import Foundation
import AuthenticationServices

protocol OAuthProvider {
    var url: URL { get }
    var callbackURLScheme: String? { get }
    var errorHandler: ((Error)->Void)? { get }
    func resolveToken(for queryItems: [URLQueryItem]) ->String
}

struct OAuthManager<A: OAuthProvider> {
    private var provider: A
    private(set) var session: ASWebAuthenticationSession?
    
    init(provider: A, contextProvider: ASWebAuthenticationPresentationContextProviding?, completion: @escaping (String)->Void) {
        self.provider = provider
        self.session = ASWebAuthenticationSession(url: provider.url, callbackURLScheme: provider.callbackURLScheme, completionHandler: { (url, error) in
            if let error = error {
                provider.errorHandler?(error)
            } else if let url = url {
                let components = URLComponents(string: url.absoluteString)
                if let queryItems = components?.queryItems {
                    let code = provider.resolveToken(for: queryItems)
                    completion(code)
                } else if let fragment = components?.fragment {
                    var components = URLComponents()
                    components.query = fragment
                    if let queryItem = components.queryItems {
                        let code = provider.resolveToken(for: queryItem)
                        completion(code)
                    }
                }
            }
        })
        session?.presentationContextProvider = contextProvider
    }
    
    func login() {
        self.session?.start()
    }
    
    func cancel() {
        self.session?.cancel()
    }
}
