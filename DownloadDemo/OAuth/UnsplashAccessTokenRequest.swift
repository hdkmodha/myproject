//
//  UnsplashAccessTokenRequest.swift
//  DownloadDemo
//
//  Created by macbook pro on 08/08/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation

struct UnsplashAccessTokenRequest: Encodable {
    let code: String
    let client_secret: String = UnsplashConstant.secretKey
    let client_id: String = UnsplashConstant.accessKey
    let redirect_uri: String = UnsplashConstant.redirectUri
    var grant_type: String
    
    init(code: String, grantType: String = "authorization_code") {
        self.code = code
        self.grant_type = grantType
    }
}

extension UnsplashAccessTokenRequest {
    
    func fetchToken(completion: @escaping (Result<String>) -> Void) {
        
        guard let parameter = self.jsonEncoded else {
            return
        }
        
        let webResource = WebResource<UnsplashTokenResponse>(urlPath: .authorization, httpMethod: .post(parameter), header: nil, decode: UnsplashTokenResponse.decode)
        
        webResource.request { (result) in
            switch result {
            case .success(let response):
                print("Response: \(response)")
                completion(.success("Token stored.."))
                response.store()
            case .failure(let error):
                print("Error: \(error.title)")
                completion(.failure(error))
            }
        }
    }
}



