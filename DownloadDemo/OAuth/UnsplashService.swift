//
//  UnsplashService.swift
//  DownloadDemo
//
//  Created by macbook pro on 08/08/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation
import Moya

enum UnsplashService {
    case photos
    case accessToken(UnsplashAccessTokenRequest)
    case getPhotos
}

extension UnsplashService: TargetType {
    var baseURL: URL {
        switch self {
        case .photos:
            return URL(string: "https://api.unsplash.com")!
        case .accessToken:
            return URL(string: "https://unsplash.com")!
        case .getPhotos:
            return URL(string: "https://staging.zoominqa.com:8080")!
        }
        
    }
    
    var path: String {
        switch self {
        case .photos:
            return "/photos"
        case .accessToken:
            return "/oauth/token"
        case .getPhotos:
            return "/api/v3/product-list"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .photos:
            return .get
        case .accessToken:
            return .post
        case .getPhotos:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .photos:
            return .requestPlain
        case .accessToken(let request):
            return .requestJSONEncodable(request)
        case .getPhotos:
            let appRequest = AppRequest(page: 0, user_agent: "mobile", pageSize: 100, country_id: 1, category_id: 0)
            return .requestJSONEncodable(appRequest)
        }
    }
    
    var headers: [String : String]? {
        
        switch self {
        case .photos:
            return ["Authorization": "Client-ID \(UnsplashConstant.accessKey)"]
        default:
            return nil
        }
        
    }
    
}

struct AppRequest: Codable {
    var page: Int?
    var user_agent: String?
    var pageSize: Int?
    var country_id: Int?
    var category_id: Int?
    
}
