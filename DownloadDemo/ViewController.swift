//
//  ViewController.swift
//  DownloadDemo
//
//  Created by macbook pro on 08/08/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var dataSource: [Photos] = []
    
    private var numberOfColumns: CGFloat = 1
    
    private lazy var flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 8
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .systemGroupedBackground
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        self.view.addSubview(collectionView)
        collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        collectionView.delegate = self
        collectionView.registerNib(ImageCell.self)
        
        return collectionView
    }()
    
    private lazy var diffableDataSource: UICollectionViewDiffableDataSource<Int, Photos> = {
        let dataSource = UICollectionViewDiffableDataSource<Int, Photos>(collectionView: self.collectionView) { (collectionView, indexPath, item) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(ImageCell.self, for: indexPath)
            cell.configure(photo: item)
            return cell
        }
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        getPhotos()
        setupDataSource()
        fetchPhotos()
    }
    
    private func setupDataSource() {
        var snapshot = NSDiffableDataSourceSnapshot<Int, Photos>()
        snapshot.appendSections([0])
        snapshot.appendItems(self.dataSource, toSection: 0)
        
        self.diffableDataSource.apply(snapshot)
    }
    
    @IBAction func gridAction(_ sender: UIBarButtonItem) {
        self.numberOfColumns = numberOfColumns == 1 ? 2 : 1
        self.collectionView.setCollectionViewLayout(self.flowLayout, animated: true)
        self.collectionView.reloadData()
    }
    
    @IBAction func loginAction(_ sender: UIBarButtonItem) {
        let provider = UnsplashAuthProvider(errorHandler: { (error) in
            print(error)
        })
        let manager = OAuthManager(provider: provider, contextProvider: self) { [weak self] (code) in
            self?.fetchAccessToken(for: code)
        }
        manager.login()
    }
    

    

}

// MARK: - WebAPI Call
extension ViewController {
    
    private func fetchAccessToken(for code: String) {
        let request = UnsplashAccessTokenRequest(code: code)
        request.fetchToken { (result) in
            switch result {
            case.success(let message):
                print("message: \(message)")
            case .failure(let error):
                self.showAlert(title: error.title)
            }
            
        }
    }
    
    private func getPhotos() {
        UnsplashResource.shared.getPhtos()
    }
    
    private func fetchPhotos() {
        UnsplashResource.shared.photos { [weak self] (result) in
            switch result {
            case .success(let photos):
                self?.dataSource = photos
                self?.setupDataSource()
                break
            case .failure(let error):
                print(error.title)
            }
        }
    }
}

// MARK: - UICollectionViewDelegate
extension ViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let item = self.dataSource[indexPath.item]
        let iHeight = CGFloat(item.height ?? 1)
        let iWidth = CGFloat(item.width ?? 1)
        let ratio = iWidth / iHeight
        
        var width = collectionView.frame.width - 16
        
        width -= self.flowLayout.minimumInteritemSpacing * (self.numberOfColumns - 1)
        width /= numberOfColumns
        width = width.rounded(.down)
        
        let height = width / ratio
        
        return CGSize(width: width, height: height)
    }
}

import AuthenticationServices
extension ViewController: ASWebAuthenticationPresentationContextProviding {
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return ASPresentationAnchor(windowScene: self.view.window!.windowScene!)
    }
    
    
}
