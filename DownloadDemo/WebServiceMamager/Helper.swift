//
//  Helper.swift
//  WebCluesPracticle
//
//  Created by macbook pro on 21/07/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation

typealias JSONType = [String: Any]
typealias Header = [String: String]
typealias ResultHandler<A> = (Result<A>) -> Void
typealias EmptyHandler = () -> Void
typealias Progress = (Double) -> Void
typealias CofirmationHandler = (Bool) -> Void
typealias ResponseStatusHandler = (Bool, String) 

