//
//  URLScheme.swift
//  ChatApp
//
//  Created by macbook pro on 19/01/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation

enum URLScheme: String {
    case http
    case https
}
//  https://unsplash.com/oauth/.
enum URLHost {
    case live
    case local
    
    var baseURL: String {
       return "unsplash.com"
    }
    
    var scheme: URLScheme {
        return .https
    }
    
    var fixedPath: String {
        return "/"
    }
}

enum URLPath {
    case authorization
    
    
    var endPoint: String {
        switch self {
        case .authorization:
            return "oauth/token"
        }
    }
    
    
    var url: URL? {
        var urlComponets = URLComponents()
        urlComponets.scheme = AppConfig.host.scheme.rawValue
        urlComponets.host = AppConfig.host.baseURL
        urlComponets.path = AppConfig.host.fixedPath + self.endPoint
        return urlComponets.url
    }
}
