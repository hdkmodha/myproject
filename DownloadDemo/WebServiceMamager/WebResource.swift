//
//  WebResource.swift
//  ChatApp
//
//  Created by macbook pro on 26/01/20.
//  Copyright © 2020 macbook pro. All rights reserved.
//

import Foundation
import Alamofire

enum HTTPMethod {
    case get
    case post(JSONType)
    case put(JSONType)
    
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .get:
            return Alamofire.HTTPMethod.get
        case .post(_):
            return Alamofire.HTTPMethod.post
        case .put(_):
            return Alamofire.HTTPMethod.put
        }
    }
    
    var parameter: JSONType? {
        switch self {
        case .post(let parameter):
            return parameter
        case .put(let parameter):
            return parameter
        default:
            return nil
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .get:
            return JSONEncoding.default
        case .put(_), .post(_):
            return URLEncoding.default
        }
    }
}

class RequestToken {
    let task: DataRequest
    
    init(task: DataRequest) {
        self.task = task
    }
    
    func cancel() {
        self.task.cancel()
    }
}


struct WebResource<A> {
    var urlPath: URLPath
    var httpMethod: HTTPMethod = .get
    var header: Header?
    var decode: (Data) -> Result<A>
    
    @discardableResult
    func request(completion: @escaping ResultHandler<A>)  -> RequestToken? {
        return WebserviceManager.shared.fetch(resource: self, completion: completion)
    }
    
    func  uploadRequest(progress: Progress?, completion: @escaping ResultHandler<A> )  {
        WebserviceManager.shared.postResource(self, progressCompletion: progress, completion: completion)
    }
    
    
}




